class MTNApp {
  String AppName="Shyft";
  String Category="Overall";
  String developers= "Standard Bank";
  String year= "2017";
  MTNApp(this.AppName, this.Category, this.developers, this.year);
  
  void printStatements (String AppName){
    print(AppName.toUpperCase());
  }

  @override
  String toString() {
    return 'MTNAPP ${AppName}, Category: ${Category},Developers${developers}, Year${year}}';
  }
}
